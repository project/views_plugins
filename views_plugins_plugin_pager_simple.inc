<?php
/**
 * @file
 * A simple pager plugin.
 */

class views_plugins_plugin_pager_simple extends views_plugin_pager_full {
  //function use_pager() {
    //return TRUE;
  //}

  // Don't execute the count query... Awesome.
  function execute_count_query(&$count_query, $args) {
  }

  function render($input) {
    $pager_theme = views_theme_functions('pager_simple', $this->view, $this->display);
    return theme($pager_theme, $input, $this->options['items_per_page'], $this->options['id'], array(), 9);
  }

  function query() {
    parent::query();

    $next_page = TRUE;
    if (!empty($this->options['total_pages'])) {
      if (($this->current_page + 1) >= $this->options['total_pages']) {
        $next_page = FALSE;
      }
    }
    if ($next_page) {
      $limit = $this->options['items_per_page'];
      $this->view->query->set_limit($limit + 1);
    }
  }

  function post_execute(&$result) {
    if (count($result) > $this->options['items_per_page']) {
      array_pop($result);
      global $pager_page_array, $pager_total, $pager_total_items;
      $pager_total[$this->options['id']] = $pager_page_array[$this->options['id']] +1;
    }
  }
}
