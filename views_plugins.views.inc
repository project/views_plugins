<?php
/**
 * @file
 * TODO
 */

/**
 * Implements hook_views_plugins().
 */
function views_plugins_views_plugins() {
  return array(
    'pager' => array(
      'simple' => array(
        'title' => t('Display a simple pager'),
        'help' => t('Display a incredible simple pager'),
        'handler' => 'views_plugins_plugin_pager_simple',
        'help topic' => 'pager-simple',
        'uses options' => TRUE,
        'parent' => 'full',
      ),
    ),
  );
}

/**
 * Implements hook_views_data_alter().
 *
 * TODO Fix naming of handler.
 */
function views_plugins_views_data_alter(&$data) {
  $data['views']['collapsible_area'] = array(
    'title' => t('Collabsible Text area'),
    'help' => t('Provide collabsible markup text for the area.'),
    'area' => array(
      'handler' => 'views_plugins_handler_collapsible_area_text',
    ),
  );
}
/**
 * Implements hook_views_handlers().
 */
function views_plugins_views_handlers() {
  return array(
    'handlers' => array(
      'views_plugins_handler_collapsible_area_text' => array (
        'parent' => 'views_handler_area_text',
      ),
    ),
  );
}

