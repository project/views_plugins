<?php
/**
 * @file
 * Provide a collapsible text area.
 */
class views_plugins_handler_collapsible_area_text extends views_handler_area_text {
  function option_definition() {
    $options = parent::option_definition();
    $options['collapsed'] = array('default' => FALSE);
    $options['collapsible'] = array('default' => TRUE);
    $options['title'] = array('default' => '', 'translateable' => TRUE);

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['title'] = array(
      '#title' => t('Titel'),
      '#type' => 'textfield',
      '#default_value' => $this->options['title'],
      '#description' => t('Title of the fieldset'),
    );
    $form['collapsed'] = array(
      '#title' => t('Collapsed'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['collapsed']),
      '#description' => t('Should the textarea be collapsed?'),
    );
    $form['collapsible'] = array(
      '#title' => t('Collapsible'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['collapsible']),
      '#description' => t('Should the textarea be collapsible'),
    );
  }

  function render($empty = FALSE) {
    if ($render = parent::render($empty)) {
      $element = array(
        '#type' => 'fieldset',
        '#title' => $this->options['title'],
        '#collapsible' => !empty($this->options['collapsible']),
        '#collapsed' => !empty($this->options['collapsed']),
        '#value' => $render,
      );
      $output = theme('fieldset', $element);
      return $output;
    }
    if (!$empty || !empty($this->options['empty'])) {
      return $this->render_textarea($this->options['content'], $this->options['format']);
    }
    return '';
  }
}
